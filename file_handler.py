import datetime
import tarfile

import jsonlines
from loguru import logger

import settings
from settings import TEMP_NAME

APPEND = "a"
WRITE = "w"


def write_statistics(day: str, number: int) -> None:
    """
    Write statistics to file
    :param day:     yyyy-mm-dd  format  (str)
    :param number:  number of ads for the given day (int)
    :return:    none
    """
    with open(settings.STATISTICS_FILE, mode=APPEND) as f:
        f.write(f"{day}, {number}\n")


def write_processed_dates(day: str) -> None:
    """
    Write the last day that was processed
    :param day: str  yyyy-mm-dd  format
    :return: none
    """
    with open(settings.PROCESSED_DATES_FILE, mode=APPEND) as f:
        f.write(f"{day}\n")


def init_files() -> None:
    """
    Create files, or overwrite if the file exist.
    """
    with jsonlines.open(settings.OUTPUT_FILE_NAME, mode=WRITE) as f:
        pass
    with open(settings.STATISTICS_FILE, mode=WRITE) as f:
        pass
    with open(settings.PROCESSED_DATES_FILE, mode=WRITE) as f:
        pass
    with open(settings.ERROR_LOG_FILE, mode=WRITE) as f:
        f.write(f"Start {datetime.datetime.now()}\n\n")
    logger.info(f"Initialized files in {settings.OUTPUT_DIR}")


def get_date_to_start_from() -> datetime.datetime:
    """
    Get the earliest date from the file with processed dates.
    :return:  datetime.datetime
    """
    datetime_to_return = datetime.datetime(year=int(settings.YEAR), month=12, day=31)
    default_value_str = datetime_to_return.strftime('%Y-%m-%d')

    if not settings.CONTINUE_FROM_PREVIOUS_RUN:
        logger.info(f"CONTINUE_FROM_PREVIOUS_RUN is False. Returning: {default_value_str}")
        return datetime_to_return
    else:
        logger.info("CONTINUE_FROM_PREVIOUS_RUN is True. Looking for the earliest date from previous run.")

    if not settings.OUTPUT_FILE_NAME.exists():
        logger.info(f"No previous output file found. Returning: {default_value_str}")
        return datetime_to_return

    with open(settings.PROCESSED_DATES_FILE, mode="r") as f:
        dates = f.readlines()
        dates = [date.strip() for date in dates]
        if len(dates) == 0:
            logger.info(f"No dates found in {settings.PROCESSED_DATES_FILE}. Returning: {default_value_str}")
            return datetime_to_return

        earliest_date = min(dates)

        datetime_to_return = datetime.datetime.strptime(earliest_date, "%Y-%m-%d")
        datetime_to_return = datetime_to_return - datetime.timedelta(days=1)
        logger.info(f"Found date {earliest_date} from previous run in {settings.PROCESSED_DATES_FILE}. Will continue from: {datetime_to_return}")

    return datetime_to_return


def append_to_output_file(ads: list) -> None:
    """
    Append ads to output file
    :param ads:
    :return: none
    """
    with jsonlines.open(settings.OUTPUT_FILE_NAME, mode=APPEND) as f:
        for ad in ads:
            f.write(ad)
    logger.info(f"Wrote {len(ads)} ads to {settings.OUTPUT_FILE_NAME}")


def write_error_file(e: Exception) -> None:
    with open(settings.ERROR_LOG_FILE, mode=APPEND) as f:
        f.write(f"{datetime.datetime.now()}: {e}\n")


def unpack_tar_gz() -> bool:
    try:
        with tarfile.open(TEMP_NAME, "r:gz") as tar:
            tar.extractall()
            logger.info("unpacked file")
            return True
    except Exception as e:
        logger.error(e)
        write_error_file(e)
        return False


def read_ads_from_daily_file() -> list:
    """
    Read ads from the daily file
    :return:  list of ads
    """
    ads = []
    with jsonlines.open("jobtechdev/minio/arkiv/output.json") as f:
        for ad in f:
            if ad["isValid"]:
                ads.append(ad)
    logger.info(f"Read {len(ads)} ads ")
    return ads
