import datetime

from loguru import logger

import file_handler
import settings
from file_handler import (init_files, append_to_output_file,
                          read_ads_from_daily_file)
from get_file import get_and_save_file

if settings.YEAR is None:
    raise ValueError("Environment variable YEAR must be set.")

year = int(settings.YEAR)

processed_ids = set()


def main_loop() -> None:
    """
    Main loop. Get ads from the last day of the year, and go backwards in time
    until we reach the first day of the year
    :return:
    """

    # start with last day of the year and go forward
    date_to_process = file_handler.get_date_to_start_from()

    while date_to_process.year == year:
        yyyy_mm_dd = date_to_process.strftime("%Y-%m-%d")
        url = f"https://data.jobtechdev.se/annonser/jobtechlinks/{yyyy_mm_dd}.tar.gz"
        logger.info(f"Start processing {url}")

        get_result = get_and_save_file(url)

        # change date to previous day
        date_to_process = date_to_process - datetime.timedelta(days=1)

        if not get_result:  # something went wrong with download or unpacking
            continue

        ads = read_ads_from_daily_file()
        ads_to_save = []
        for ad in ads:
            if ad["id"] not in processed_ids:
                ads_to_save.append(ad)
                processed_ids.add(ad["id"])
            else:
                """
                already saved, and since we go backwards in time the latest
                version of the ad is saved to file
                """
                pass

        append_to_output_file(ads_to_save)
        file_handler.write_statistics(yyyy_mm_dd, len(ads))
        file_handler.write_processed_dates(yyyy_mm_dd)

        logger.info(f"Finished processing {url}")


if __name__ == '__main__':
    if not settings.CONTINUE_FROM_PREVIOUS_RUN:
        init_files()
    main_loop()
    logger.info(f"Finished. Saved {len(processed_ids)} ads")
