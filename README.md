# Create historical files for JobAd Links

## Summary

This program downloads files day by day from files hosted on https://data.jobtechdev.se/annonser/jobtechlinks/ and consolidate
them into a yearly file that is manually uploaded to data.jobtechdev.se/historiska/jobtechlinks

## Steps

Start the program, it does the following:
1. Gets the latest JobAd Links zip file for a year from data.jobtechdev.se. This is a file matching "YYYY-12-31"
2. Unpacks the zip file and write the ads to an output file. Ad ids are saved to a set in memory.
3. Downloads the zip file from the previous day ("YYYY-12-30"), and unpacks it. It reads all ads, if the ad's ad is in the set of
   already processed ids, the ad is ignored.
4. Continues downloading and process files backwards in time until the file's file name no longer match they pattern for
   the year.

When the program is finished, there are some manual work that needs to be done:
- Compress (zip) the file (for example jobad_links_2023.zip)  
- Upload the zip-file to https://data.jobtechdev.se/annonser/historiska/jobad_links/ by    
  login to AWS S3: https://s3.console.aws.amazon.com/s3/buckets/data.jobtechdev.se-adhoc?region=eu-north-1&prefix=annonser/historiska/jobad_links/&showversions=false
  and use web GUI to upload the file
  After upload to AWS S3 it will take about 15-20 minutes before the file shows up in https://data.jobtechdev.se/annonser/historiska/jobad_links/
- Write a post in JobTech forum that a file is available.

## Install dependencies

This program requires Python version 3.11 or later. It uses `poetry` for dependency management. Install Poetry
with `python -m pip install poetry`.  
When you have installed Poetry you can install the dependencies into a virtual environment with the
command: `poetry install`

### How to run

There is no openshift configuration, run it from your computer.
set the environment variable YEAR (as YYYY)
run `python main.py`

### Output

Default output folder `output` is created and the following files are saved there. If you run the program again, these
files are overwritten.

- jobad_links_YYYY.jsonl
- errors_YYYY.txt
- statistics_YYYY.csv (yyyy-mm-dd, number of ads in the file that day, no duplicate removal)
-

### Duplicate handling

A specific ad will be found in files for several dates. This program saves the last version of the ad. If previous
versions (ad is found in a file for an earlier date), are found, they are ignored.
But since this program takes calendar years, an ad might be found in two years.
E.g. An ad is published 2023-12-08 and will be included in the summary file for 2023.
But it's not unpublished until 2024-01-30, so it will show up in the summary file for 2024 as well.
This is the same problem as in Historical Ads from JobSearch.

### Error handling

Not much. Errors are written to a file.
In case it stops working, extract the ads from the existing ads file and write some code to load the ad ids into the
set `processed_ads`.
Adjust starting date to continue where it stopped working.

Or just run everything again.

### Possible improvements:
Error handling and option to resume
See "issues" in this repo

### Running it in Openshift

This would (at least) require:
A yearly cronjob
Temporary storage for the downloaded files
Some way of uploading the result to data.jobtechdev.se
Notifications for a failed pipeline

### Known issues

For 2021, ad files are available from 2021-03-31 when JobAd links went into production.  
Some days do not have files, this will be logged and the program will continue with the next file.
This program will take some time to run. The duplicate handling by using the last version makes it hard to do anything
else than sequential processing.
