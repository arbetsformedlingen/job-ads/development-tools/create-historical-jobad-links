import requests
import time
from loguru import logger

from file_handler import write_error_file, unpack_tar_gz
from settings import TEMP_NAME

RETRIES = 10


def get_and_save_file(url: str) -> bool:

    for retry in range(RETRIES):
        try:
            response = requests.get(url, stream=True)
            response.raise_for_status()
        except Exception as e:
            if retry + 1 == RETRIES:
                logger.error(f"Exception for {url}: {e}")
                write_error_file(e)
                return False
            else:
                logger.warning(f"get_and_save_file() retrying #{retry + 1} after error: {e}")
                time.sleep(retry)  # longer and longer sleep
        else:
            # if GET is successful, unpack the tar.gz file
            with open(TEMP_NAME, mode='wb') as f:
                f.write(response.raw.read())
                unpack_result = unpack_tar_gz()
                return unpack_result
