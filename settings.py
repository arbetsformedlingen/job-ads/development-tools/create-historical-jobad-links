import os

import pathlib

YEAR = os.getenv("YEAR")

"""
CONTINUE_FROM_PREVIOUS_RUN - True if you want to continue from the previous
run, False if you want to start from scratch. Default is True
"""
CONTINUE_FROM_PREVIOUS_RUN = os.getenv("CONTINUE_FROM_PREVIOUS_RUN",
                                       "true").lower() == "true"

OUTPUT_DIR = pathlib.Path("output")
OUTPUT_DIR.mkdir(exist_ok=True)

ERROR_LOG_FILE = OUTPUT_DIR / f"errors_{YEAR}.txt"

OUTPUT_FILE_NAME = OUTPUT_DIR / f"jobad_links_{YEAR}.jsonl"

STATISTICS_FILE = OUTPUT_DIR / f"statistics_{YEAR}.csv"

PROCESSED_DATES_FILE = OUTPUT_DIR / f"processed_dates_{YEAR}.csv"

TEMP_NAME = "temp.tar.gz"
